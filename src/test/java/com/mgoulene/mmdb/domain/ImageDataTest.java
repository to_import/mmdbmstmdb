package com.mgoulene.mmdb.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.mgoulene.mmdb.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class ImageDataTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ImageData.class);
        ImageData imageData1 = new ImageData();
        imageData1.setId("id1");
        ImageData imageData2 = new ImageData();
        imageData2.setId(imageData1.getId());
        assertThat(imageData1).isEqualTo(imageData2);
        imageData2.setId("id2");
        assertThat(imageData1).isNotEqualTo(imageData2);
        imageData1.setId(null);
        assertThat(imageData1).isNotEqualTo(imageData2);
    }
}
