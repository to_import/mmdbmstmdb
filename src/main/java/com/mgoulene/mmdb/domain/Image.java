package com.mgoulene.mmdb.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.LocalDate;
import javax.validation.constraints.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 * The Image entity.\n@author A true hipster
 */
@Document(collection = "image")
public class Image implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @NotNull
    @Field("tmdb_id")
    private String tmdbId;

    @Field("locale")
    private String locale;

    @Field("vote_average")
    private Float voteAverage;

    @Field("vote_count")
    private Integer voteCount;

    @NotNull
    @Field("last_tmdb_update")
    private LocalDate lastTMDBUpdate;

    @DBRef
    @Field("posterMovie")
    @JsonIgnoreProperties(value = { "credits", "posters", "backdrops", "genres" }, allowSetters = true)
    private Movie posterMovie;

    @DBRef
    @Field("backdropMovie")
    @JsonIgnoreProperties(value = { "credits", "posters", "backdrops", "genres" }, allowSetters = true)
    private Movie backdropMovie;

    @DBRef
    @Field("person")
    @JsonIgnoreProperties(value = { "profiles" }, allowSetters = true)
    private Person person;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Image id(String id) {
        this.id = id;
        return this;
    }

    public String getTmdbId() {
        return this.tmdbId;
    }

    public Image tmdbId(String tmdbId) {
        this.tmdbId = tmdbId;
        return this;
    }

    public void setTmdbId(String tmdbId) {
        this.tmdbId = tmdbId;
    }

    public String getLocale() {
        return this.locale;
    }

    public Image locale(String locale) {
        this.locale = locale;
        return this;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public Float getVoteAverage() {
        return this.voteAverage;
    }

    public Image voteAverage(Float voteAverage) {
        this.voteAverage = voteAverage;
        return this;
    }

    public void setVoteAverage(Float voteAverage) {
        this.voteAverage = voteAverage;
    }

    public Integer getVoteCount() {
        return this.voteCount;
    }

    public Image voteCount(Integer voteCount) {
        this.voteCount = voteCount;
        return this;
    }

    public void setVoteCount(Integer voteCount) {
        this.voteCount = voteCount;
    }

    public LocalDate getLastTMDBUpdate() {
        return this.lastTMDBUpdate;
    }

    public Image lastTMDBUpdate(LocalDate lastTMDBUpdate) {
        this.lastTMDBUpdate = lastTMDBUpdate;
        return this;
    }

    public void setLastTMDBUpdate(LocalDate lastTMDBUpdate) {
        this.lastTMDBUpdate = lastTMDBUpdate;
    }

    public Movie getPosterMovie() {
        return this.posterMovie;
    }

    public Image posterMovie(Movie movie) {
        this.setPosterMovie(movie);
        return this;
    }

    public void setPosterMovie(Movie movie) {
        this.posterMovie = movie;
    }

    public Movie getBackdropMovie() {
        return this.backdropMovie;
    }

    public Image backdropMovie(Movie movie) {
        this.setBackdropMovie(movie);
        return this;
    }

    public void setBackdropMovie(Movie movie) {
        this.backdropMovie = movie;
    }

    public Person getPerson() {
        return this.person;
    }

    public Image person(Person person) {
        this.setPerson(person);
        return this;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Image)) {
            return false;
        }
        return id != null && id.equals(((Image) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Image{" +
            "id=" + getId() +
            ", tmdbId='" + getTmdbId() + "'" +
            ", locale='" + getLocale() + "'" +
            ", voteAverage=" + getVoteAverage() +
            ", voteCount=" + getVoteCount() +
            ", lastTMDBUpdate='" + getLastTMDBUpdate() + "'" +
            "}";
    }
}
