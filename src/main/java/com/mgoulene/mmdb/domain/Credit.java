package com.mgoulene.mmdb.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.LocalDate;
import javax.validation.constraints.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 * The Credit entity.\n@author A true hipster
 */
@Document(collection = "credit")
public class Credit implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    /**
     * fieldName
     */
    @NotNull
    @Field("tmdb_id")
    private String tmdbId;

    @Size(max = 200)
    @Field("character")
    private String character;

    @Field("credit_type")
    private String creditType;

    @Field("department")
    private String department;

    @Field("job")
    private String job;

    @Field("order")
    private Integer order;

    @NotNull
    @Field("last_tmdb_update")
    private LocalDate lastTMDBUpdate;

    @DBRef
    @Field("person")
    @JsonIgnoreProperties(value = { "profiles" }, allowSetters = true)
    private Person person;

    @DBRef
    @Field("movie")
    @JsonIgnoreProperties(value = { "credits", "posters", "backdrops", "genres" }, allowSetters = true)
    private Movie movie;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Credit id(String id) {
        this.id = id;
        return this;
    }

    public String getTmdbId() {
        return this.tmdbId;
    }

    public Credit tmdbId(String tmdbId) {
        this.tmdbId = tmdbId;
        return this;
    }

    public void setTmdbId(String tmdbId) {
        this.tmdbId = tmdbId;
    }

    public String getCharacter() {
        return this.character;
    }

    public Credit character(String character) {
        this.character = character;
        return this;
    }

    public void setCharacter(String character) {
        this.character = character;
    }

    public String getCreditType() {
        return this.creditType;
    }

    public Credit creditType(String creditType) {
        this.creditType = creditType;
        return this;
    }

    public void setCreditType(String creditType) {
        this.creditType = creditType;
    }

    public String getDepartment() {
        return this.department;
    }

    public Credit department(String department) {
        this.department = department;
        return this;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getJob() {
        return this.job;
    }

    public Credit job(String job) {
        this.job = job;
        return this;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public Integer getOrder() {
        return this.order;
    }

    public Credit order(Integer order) {
        this.order = order;
        return this;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    public LocalDate getLastTMDBUpdate() {
        return this.lastTMDBUpdate;
    }

    public Credit lastTMDBUpdate(LocalDate lastTMDBUpdate) {
        this.lastTMDBUpdate = lastTMDBUpdate;
        return this;
    }

    public void setLastTMDBUpdate(LocalDate lastTMDBUpdate) {
        this.lastTMDBUpdate = lastTMDBUpdate;
    }

    public Person getPerson() {
        return this.person;
    }

    public Credit person(Person person) {
        this.setPerson(person);
        return this;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public Movie getMovie() {
        return this.movie;
    }

    public Credit movie(Movie movie) {
        this.setMovie(movie);
        return this;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Credit)) {
            return false;
        }
        return id != null && id.equals(((Credit) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Credit{" +
            "id=" + getId() +
            ", tmdbId='" + getTmdbId() + "'" +
            ", character='" + getCharacter() + "'" +
            ", creditType='" + getCreditType() + "'" +
            ", department='" + getDepartment() + "'" +
            ", job='" + getJob() + "'" +
            ", order=" + getOrder() +
            ", lastTMDBUpdate='" + getLastTMDBUpdate() + "'" +
            "}";
    }
}
