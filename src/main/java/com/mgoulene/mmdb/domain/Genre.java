package com.mgoulene.mmdb.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import javax.validation.constraints.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 * The Genre entity.\n@author A true hipster
 */
@Document(collection = "genre")
public class Genre implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @NotNull
    @Field("tmdb_id")
    private String tmdbId;

    @NotNull
    @Field("name")
    private String name;

    @NotNull
    @Field("last_tmdb_update")
    private LocalDate lastTMDBUpdate;

    @DBRef
    @Field("movies")
    @JsonIgnoreProperties(value = { "credits", "posters", "backdrops", "genres" }, allowSetters = true)
    private Set<Movie> movies = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Genre id(String id) {
        this.id = id;
        return this;
    }

    public String getTmdbId() {
        return this.tmdbId;
    }

    public Genre tmdbId(String tmdbId) {
        this.tmdbId = tmdbId;
        return this;
    }

    public void setTmdbId(String tmdbId) {
        this.tmdbId = tmdbId;
    }

    public String getName() {
        return this.name;
    }

    public Genre name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getLastTMDBUpdate() {
        return this.lastTMDBUpdate;
    }

    public Genre lastTMDBUpdate(LocalDate lastTMDBUpdate) {
        this.lastTMDBUpdate = lastTMDBUpdate;
        return this;
    }

    public void setLastTMDBUpdate(LocalDate lastTMDBUpdate) {
        this.lastTMDBUpdate = lastTMDBUpdate;
    }

    public Set<Movie> getMovies() {
        return this.movies;
    }

    public Genre movies(Set<Movie> movies) {
        this.setMovies(movies);
        return this;
    }

    public Genre addMovie(Movie movie) {
        this.movies.add(movie);
        movie.getGenres().add(this);
        return this;
    }

    public Genre removeMovie(Movie movie) {
        this.movies.remove(movie);
        movie.getGenres().remove(this);
        return this;
    }

    public void setMovies(Set<Movie> movies) {
        if (this.movies != null) {
            this.movies.forEach(i -> i.removeGenre(this));
        }
        if (movies != null) {
            movies.forEach(i -> i.addGenre(this));
        }
        this.movies = movies;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Genre)) {
            return false;
        }
        return id != null && id.equals(((Genre) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Genre{" +
            "id=" + getId() +
            ", tmdbId='" + getTmdbId() + "'" +
            ", name='" + getName() + "'" +
            ", lastTMDBUpdate='" + getLastTMDBUpdate() + "'" +
            "}";
    }
}
