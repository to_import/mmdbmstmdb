package com.mgoulene.mmdb.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import javax.validation.constraints.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 * A ImageData.
 */
@Document(collection = "image_data")
public class ImageData implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @NotNull
    @Size(max = 40)
    @Field("image_size")
    private String imageSize;

    @Field("image_bytes")
    private byte[] imageBytes;

    @Field("image_bytes_content_type")
    private String imageBytesContentType;

    @DBRef
    @Field("image")
    @JsonIgnoreProperties(value = { "posterMovie", "backdropMovie", "person" }, allowSetters = true)
    private Image image;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ImageData id(String id) {
        this.id = id;
        return this;
    }

    public String getImageSize() {
        return this.imageSize;
    }

    public ImageData imageSize(String imageSize) {
        this.imageSize = imageSize;
        return this;
    }

    public void setImageSize(String imageSize) {
        this.imageSize = imageSize;
    }

    public byte[] getImageBytes() {
        return this.imageBytes;
    }

    public ImageData imageBytes(byte[] imageBytes) {
        this.imageBytes = imageBytes;
        return this;
    }

    public void setImageBytes(byte[] imageBytes) {
        this.imageBytes = imageBytes;
    }

    public String getImageBytesContentType() {
        return this.imageBytesContentType;
    }

    public ImageData imageBytesContentType(String imageBytesContentType) {
        this.imageBytesContentType = imageBytesContentType;
        return this;
    }

    public void setImageBytesContentType(String imageBytesContentType) {
        this.imageBytesContentType = imageBytesContentType;
    }

    public Image getImage() {
        return this.image;
    }

    public ImageData image(Image image) {
        this.setImage(image);
        return this;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ImageData)) {
            return false;
        }
        return id != null && id.equals(((ImageData) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ImageData{" +
            "id=" + getId() +
            ", imageSize='" + getImageSize() + "'" +
            ", imageBytes='" + getImageBytes() + "'" +
            ", imageBytesContentType='" + getImageBytesContentType() + "'" +
            "}";
    }
}
