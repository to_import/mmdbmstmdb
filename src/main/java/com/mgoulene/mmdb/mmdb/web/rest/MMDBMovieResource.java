package com.mgoulene.mmdb.mmdb.web.rest;

import com.mgoulene.mmdb.mmdb.service.MMDBMovieService;
import com.mgoulene.mmdb.service.dto.MovieDTO;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.mgoulene.mmdb.domain.Movie}.
 */
@RestController
@RequestMapping("/api")
public class MMDBMovieResource {

    private final Logger log = LoggerFactory.getLogger(MMDBMovieResource.class);

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final MMDBMovieService movieService;

    public MMDBMovieResource(MMDBMovieService movieService) {
        this.movieService = movieService;
    }

    /**
     * {@code GET  /movies/:id} : get the "id" movie.
     *
     * @param id the id of the movieDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the movieDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/movies/tmdb/{tmdbId}")
    public ResponseEntity<MovieDTO> getMovieByTmdbId(@PathVariable String tmdbId) {
        log.debug("REST request to get Movie By tmbdId: {}", tmdbId);
        Optional<MovieDTO> movieDTO = movieService.findOneByTmdbId(tmdbId);
        return ResponseUtil.wrapOrNotFound(movieDTO);
    }
}
