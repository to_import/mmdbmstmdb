package com.mgoulene.mmdb.service.dto;

import com.mgoulene.mmdb.domain.enumeration.MovieStatus;
import io.swagger.annotations.ApiModel;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link com.mgoulene.mmdb.domain.Movie} entity.
 */
@ApiModel(description = "The Movie entity.\n@author A true hipster")
public class MovieDTO implements Serializable {

    private String id;

    @Size(max = 200)
    private String title;

    private Boolean forAdult;

    private String homepage;

    private String originalLanguage;

    private String originalTitle;

    @Size(max = 4000)
    private String overview;

    private String tagline;

    private MovieStatus status;

    @DecimalMin(value = "0")
    @DecimalMax(value = "10")
    private Float voteAverage;

    private Integer voteCount;

    private LocalDate releaseDate;

    @NotNull
    private LocalDate lastTMDBUpdate;

    @NotNull
    private String tmdbId;

    private Integer runtime;

    private Set<GenreDTO> genres = new HashSet<>();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Boolean getForAdult() {
        return forAdult;
    }

    public void setForAdult(Boolean forAdult) {
        this.forAdult = forAdult;
    }

    public String getHomepage() {
        return homepage;
    }

    public void setHomepage(String homepage) {
        this.homepage = homepage;
    }

    public String getOriginalLanguage() {
        return originalLanguage;
    }

    public void setOriginalLanguage(String originalLanguage) {
        this.originalLanguage = originalLanguage;
    }

    public String getOriginalTitle() {
        return originalTitle;
    }

    public void setOriginalTitle(String originalTitle) {
        this.originalTitle = originalTitle;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public String getTagline() {
        return tagline;
    }

    public void setTagline(String tagline) {
        this.tagline = tagline;
    }

    public MovieStatus getStatus() {
        return status;
    }

    public void setStatus(MovieStatus status) {
        this.status = status;
    }

    public Float getVoteAverage() {
        return voteAverage;
    }

    public void setVoteAverage(Float voteAverage) {
        this.voteAverage = voteAverage;
    }

    public Integer getVoteCount() {
        return voteCount;
    }

    public void setVoteCount(Integer voteCount) {
        this.voteCount = voteCount;
    }

    public LocalDate getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(LocalDate releaseDate) {
        this.releaseDate = releaseDate;
    }

    public LocalDate getLastTMDBUpdate() {
        return lastTMDBUpdate;
    }

    public void setLastTMDBUpdate(LocalDate lastTMDBUpdate) {
        this.lastTMDBUpdate = lastTMDBUpdate;
    }

    public String getTmdbId() {
        return tmdbId;
    }

    public void setTmdbId(String tmdbId) {
        this.tmdbId = tmdbId;
    }

    public Integer getRuntime() {
        return runtime;
    }

    public void setRuntime(Integer runtime) {
        this.runtime = runtime;
    }

    public Set<GenreDTO> getGenres() {
        return genres;
    }

    public void setGenres(Set<GenreDTO> genres) {
        this.genres = genres;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof MovieDTO)) {
            return false;
        }

        MovieDTO movieDTO = (MovieDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, movieDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "MovieDTO{" +
            "id='" + getId() + "'" +
            ", title='" + getTitle() + "'" +
            ", forAdult='" + getForAdult() + "'" +
            ", homepage='" + getHomepage() + "'" +
            ", originalLanguage='" + getOriginalLanguage() + "'" +
            ", originalTitle='" + getOriginalTitle() + "'" +
            ", overview='" + getOverview() + "'" +
            ", tagline='" + getTagline() + "'" +
            ", status='" + getStatus() + "'" +
            ", voteAverage=" + getVoteAverage() +
            ", voteCount=" + getVoteCount() +
            ", releaseDate='" + getReleaseDate() + "'" +
            ", lastTMDBUpdate='" + getLastTMDBUpdate() + "'" +
            ", tmdbId='" + getTmdbId() + "'" +
            ", runtime=" + getRuntime() +
            ", genres='" + getGenres() + "'" +
            "}";
    }
}
