package com.mgoulene.mmdb.service.dto;

import java.io.Serializable;
import java.util.Objects;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link com.mgoulene.mmdb.domain.ImageData} entity.
 */
public class ImageDataDTO implements Serializable {

    private String id;

    @NotNull
    @Size(max = 40)
    private String imageSize;

    private byte[] imageBytes;

    private String imageBytesContentType;
    private ImageDTO image;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImageSize() {
        return imageSize;
    }

    public void setImageSize(String imageSize) {
        this.imageSize = imageSize;
    }

    public byte[] getImageBytes() {
        return imageBytes;
    }

    public void setImageBytes(byte[] imageBytes) {
        this.imageBytes = imageBytes;
    }

    public String getImageBytesContentType() {
        return imageBytesContentType;
    }

    public void setImageBytesContentType(String imageBytesContentType) {
        this.imageBytesContentType = imageBytesContentType;
    }

    public ImageDTO getImage() {
        return image;
    }

    public void setImage(ImageDTO image) {
        this.image = image;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ImageDataDTO)) {
            return false;
        }

        ImageDataDTO imageDataDTO = (ImageDataDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, imageDataDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ImageDataDTO{" +
            "id='" + getId() + "'" +
            ", imageSize='" + getImageSize() + "'" +
            ", imageBytes='" + getImageBytes() + "'" +
            ", image='" + getImage() + "'" +
            "}";
    }
}
