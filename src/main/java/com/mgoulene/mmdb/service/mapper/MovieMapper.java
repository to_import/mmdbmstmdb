package com.mgoulene.mmdb.service.mapper;

import com.mgoulene.mmdb.domain.*;
import com.mgoulene.mmdb.service.dto.MovieDTO;
import java.util.Set;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Movie} and its DTO {@link MovieDTO}.
 */
@Mapper(componentModel = "spring", uses = { GenreMapper.class })
public interface MovieMapper extends EntityMapper<MovieDTO, Movie> {
    @Mapping(target = "genres", source = "genres", qualifiedByName = "nameSet")
    MovieDTO toDto(Movie s);

    @Mapping(target = "removeGenre", ignore = true)
    Movie toEntity(MovieDTO movieDTO);

    @Named("title")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    @Mapping(target = "title", source = "title")
    MovieDTO toDtoTitle(Movie movie);
}
