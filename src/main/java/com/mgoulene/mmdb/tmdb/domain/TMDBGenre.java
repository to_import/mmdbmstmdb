package com.mgoulene.mmdb.tmdb.domain;

public class TMDBGenre {

    private String id;
    private String name;

    public TMDBGenre() {}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "TMDBGenreDTO [id=" + id + ", name=" + name + "]";
    }
}
