package com.mgoulene.mmdb.tmdb.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Arrays;

public class TMDBMovieCreditResponse {

    @JsonProperty
    private TMDBCredit[] cast;

    @JsonProperty
    private TMDBCredit[] crew;

    public TMDBMovieCreditResponse() {}

    public TMDBCredit[] getCast() {
        return cast;
    }

    public void setCast(TMDBCredit[] cast) {
        this.cast = cast;
    }

    public TMDBCredit[] getCrew() {
        return crew;
    }

    public void setCrew(TMDBCredit[] crew) {
        this.crew = crew;
    }

    @Override
    public String toString() {
        return "TMDBMovieCreditResponse [cast=" + Arrays.toString(cast) + ", crew=" + Arrays.toString(crew) + "]";
    }
}
