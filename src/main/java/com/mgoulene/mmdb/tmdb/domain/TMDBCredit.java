package com.mgoulene.mmdb.tmdb.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TMDBCredit {

    @JsonProperty("credit_id")
    private String tmdbId;

    private String creditType;

    @JsonProperty("character")
    private String character;

    @JsonProperty("gender")
    private Integer gender;

    @JsonProperty("id")
    private String tmdbPersonId;

    @JsonProperty("order")
    private Integer order;

    @JsonProperty("department")
    private String department;

    @JsonProperty("job")
    private String job;

    private Long movieId;

    public String getTmdbId() {
        return tmdbId;
    }

    public void setTmdbId(String tmdbId) {
        this.tmdbId = tmdbId;
    }

    public String getCreditType() {
        return creditType;
    }

    public void setCreditType(String creditType) {
        this.creditType = creditType;
    }

    public String getCharacter() {
        return character;
    }

    public void setCharacter(String character) {
        this.character = character;
    }

    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public String getTmdbPersonId() {
        return tmdbPersonId;
    }

    public void setTmdbPersonId(String tmdbPersonId) {
        this.tmdbPersonId = tmdbPersonId;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public Long getMovieId() {
        return movieId;
    }

    public void setMovieId(Long movieId) {
        this.movieId = movieId;
    }

    @Override
    public String toString() {
        return (
            "TMDBCreditDTO [character=" +
            character +
            ", creditType=" +
            creditType +
            ", department=" +
            department +
            ", gender=" +
            gender +
            ", job=" +
            job +
            ", order=" +
            order +
            ", tmdbId=" +
            tmdbId +
            ", tmdbPersonId=" +
            tmdbPersonId +
            ", movieId=" +
            movieId +
            "]"
        );
    }
}
