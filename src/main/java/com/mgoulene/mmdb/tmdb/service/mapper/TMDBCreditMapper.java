package com.mgoulene.mmdb.tmdb.service.mapper;

import com.mgoulene.mmdb.domain.Movie;
import com.mgoulene.mmdb.service.dto.CreditDTO;
import com.mgoulene.mmdb.service.dto.MovieDTO;
import com.mgoulene.mmdb.tmdb.domain.TMDBCredit;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

/**
 * Mapper for the entity {@link Movie} and its DTO {@link MovieDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface TMDBCreditMapper extends TMDBMapper<TMDBCredit, CreditDTO> {
    CreditDTO toDTO(TMDBCredit tmdbCreditDTO);

    @Mapping(target = "id", ignore = true)
    void updateDTO(TMDBCredit tmdbCreditDTO, @MappingTarget CreditDTO creditDTO);
}
