package com.mgoulene.mmdb.tmdb.service.mapper;

import com.mgoulene.mmdb.service.dto.GenreDTO;
import com.mgoulene.mmdb.tmdb.domain.TMDBGenre;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

@Mapper(componentModel = "spring", uses = {})
public interface TMDBGenreMapper extends TMDBMapper<TMDBGenre, GenreDTO> {
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "tmdbId", source = "id")
    GenreDTO toDTO(TMDBGenre tmdbGenreDTO);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "tmdbId", source = "id")
    void updateDTO(TMDBGenre tmdbGenreDTO, @MappingTarget GenreDTO genreDTO);
}
