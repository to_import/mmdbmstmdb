package com.mgoulene.mmdb.tmdb.service.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TMDBPersonDTO {

    @JsonProperty("birthday")
    private String birthday;

    @JsonProperty("known_for_department")
    private String knownForDepartment;

    @JsonProperty("deathday")
    private String deathday;

    @JsonProperty("id")
    private String tmdbId;

    @JsonProperty("name")
    private String name;

    @JsonProperty("gender")
    private Integer gender;

    @JsonProperty("biography")
    private String biography;

    @JsonProperty("popularity")
    private Double popularity;

    @JsonProperty("place_of_birth")
    private String placeOfBirth;

    @JsonProperty("profile_path")
    private String profile_path;

    @JsonProperty("homepage")
    private String homepage;

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getKnownForDepartment() {
        return knownForDepartment;
    }

    public void setKnownForDepartment(String knownForDepartment) {
        this.knownForDepartment = knownForDepartment;
    }

    public String getDeathday() {
        return deathday;
    }

    public void setDeathday(String deathday) {
        this.deathday = deathday;
    }

    public String getTmdbId() {
        return tmdbId;
    }

    public void setTmdbId(String tmdbId) {
        this.tmdbId = tmdbId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public String getBiography() {
        return biography;
    }

    public void setBiography(String biography) {
        this.biography = biography;
    }

    public Double getPopularity() {
        return popularity;
    }

    public void setPopularity(Double popularity) {
        this.popularity = popularity;
    }

    public String getPlaceOfBirth() {
        return placeOfBirth;
    }

    public void setPlaceOfBirth(String placeOfBirth) {
        this.placeOfBirth = placeOfBirth;
    }

    public String getProfile_path() {
        return profile_path;
    }

    public void setProfile_path(String profile_path) {
        this.profile_path = profile_path;
    }

    public String getHomepage() {
        return homepage;
    }

    public void setHomepage(String homepage) {
        this.homepage = homepage;
    }

    @Override
    public String toString() {
        return (
            "TMDBPersonDTO [biography=" +
            biography +
            ", birthday=" +
            birthday +
            ", deathday=" +
            deathday +
            ", gender=" +
            gender +
            ", homepage=" +
            homepage +
            ", knownForDepartment=" +
            knownForDepartment +
            ", name=" +
            name +
            ", placeOfBirth=" +
            placeOfBirth +
            ", popularity=" +
            popularity +
            ", profile_path=" +
            profile_path +
            ", tmdbId=" +
            tmdbId +
            "]"
        );
    }
}
