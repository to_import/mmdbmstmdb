package com.mgoulene.mmdb.tmdb.service.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TMDBImageDTO {

    @JsonProperty("file_path")
    private String filePath;

    private int height;
    private int width;

    @JsonProperty("iso_639_1")
    private String locale;

    @JsonProperty("vote_count")
    private Integer voteCount;

    @JsonProperty("vote_average")
    private Float voteAverage;

    public TMDBImageDTO() {}

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public Integer getVoteCount() {
        return voteCount;
    }

    public void setVoteCount(Integer voteCount) {
        this.voteCount = voteCount;
    }

    public Float getVoteAverage() {
        return voteAverage;
    }

    public void setVoteAverage(Float voteAverage) {
        this.voteAverage = voteAverage;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((filePath == null) ? 0 : filePath.hashCode());
        result = prime * result + height;
        result = prime * result + ((locale == null) ? 0 : locale.hashCode());
        result = prime * result + ((voteAverage == null) ? 0 : voteAverage.hashCode());
        result = prime * result + ((voteCount == null) ? 0 : voteCount.hashCode());
        result = prime * result + width;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        TMDBImageDTO other = (TMDBImageDTO) obj;
        if (filePath == null) {
            if (other.filePath != null) return false;
        } else if (!filePath.equals(other.filePath)) return false;
        if (height != other.height) return false;
        if (locale == null) {
            if (other.locale != null) return false;
        } else if (!locale.equals(other.locale)) return false;
        if (voteAverage == null) {
            if (other.voteAverage != null) return false;
        } else if (!voteAverage.equals(other.voteAverage)) return false;
        if (voteCount == null) {
            if (other.voteCount != null) return false;
        } else if (!voteCount.equals(other.voteCount)) return false;
        if (width != other.width) return false;
        return true;
    }

    @Override
    public String toString() {
        return (
            "TMDBImageDTO [filePath=" +
            filePath +
            ", height=" +
            height +
            ", locale=" +
            locale +
            ", voteAverage=" +
            voteAverage +
            ", voteCount=" +
            voteCount +
            ", width=" +
            width +
            "]"
        );
    }
}
