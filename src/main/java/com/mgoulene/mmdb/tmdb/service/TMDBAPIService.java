package com.mgoulene.mmdb.tmdb.service;

import com.mgoulene.mmdb.config.ApplicationProperties;
import com.mgoulene.mmdb.service.dto.ImageDataDTO;
import com.mgoulene.mmdb.service.dto.MovieDTO;
import com.mgoulene.mmdb.tmdb.domain.TMDBMovie;
import com.mgoulene.mmdb.tmdb.domain.TMDBMovieCreditResponse;
import com.mgoulene.mmdb.tmdb.service.dto.TMDBMovieImageDTO;
import com.mgoulene.mmdb.tmdb.service.dto.TMDBMovieSearchResponse;
import com.mgoulene.mmdb.tmdb.service.dto.TMDBPersonDTO;
import com.mgoulene.mmdb.tmdb.service.dto.TMDBPersonImageDTO;
import com.mgoulene.mmdb.tmdb.service.mapper.TMDBMovieMapper;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

@Service
@Transactional
public class TMDBAPIService {

    private static final String TMDB_IMAGE_URL = "https://image.tmdb.org";
    private static final String TMDB_API_URL = "https://api.themoviedb.org/3";

    private final Logger log = LoggerFactory.getLogger(TMDBAPIService.class);

    private final ApplicationProperties.TMDBProperties tmdbProps;

    private final TMDBMovieMapper tmdbMovieMapper;

    public TMDBAPIService(ApplicationProperties appProps, TMDBMovieMapper tmdbMovieMapper) {
        this.tmdbProps = appProps.getTMDB();
        this.tmdbMovieMapper = tmdbMovieMapper;
    }

    @Transactional(readOnly = true)
    public List<MovieDTO> searchTMDBMovies(String query) {
        log.debug("Request to search for TMDB Movies for query {}", query);
        RestTemplate restTemplate = new RestTemplate();
        TMDBMovieSearchResponse movies = restTemplate.getForObject(
            tmdbProps.getURLWithAPIKey(TMDB_API_URL + "/search/movie?&query=" + query),
            TMDBMovieSearchResponse.class
        );
        return tmdbMovieMapper.toDTO(Arrays.asList(movies.getResults()));
    }

    @Transactional(readOnly = true)
    public TMDBMovie findOneTMDBMovie(String tmdbId) {
        log.debug("Request to find a TMDB Movie for id {}", tmdbId);
        RestTemplate restTemplate = new RestTemplate();
        TMDBMovie tmdbMovieDTO = restTemplate.getForObject(
            tmdbProps.getURLWithAPIKey(TMDB_API_URL + "/movie/" + tmdbId + "?"),
            TMDBMovie.class
        );
        log.debug("Request to find a TMDB Movie for id {}, TMDBMovieDTO {}", tmdbId, tmdbMovieDTO);
        return tmdbMovieDTO;
    }

    @Transactional(readOnly = true)
    public TMDBPersonDTO findOneTMDBPerson(String tmdbId) {
        log.debug("Request to find a TMDB Person for id {}", tmdbId);
        RestTemplate restTemplate = new RestTemplate();
        TMDBPersonDTO tmdbPersonDTO = restTemplate.getForObject(
            tmdbProps.getURLWithAPIKey(TMDB_API_URL + "/person/" + tmdbId + "?"),
            TMDBPersonDTO.class
        );
        log.debug("Request to find a TMDB Person for id {}, TMDBMovieDTO {}, TMDBPersonDTO {} ", tmdbId, tmdbPersonDTO);
        return tmdbPersonDTO;
    }

    @Transactional(readOnly = true)
    public ImageDataDTO findOneTMDBImageData(String id, String size) {
        ImageDataDTO imageDataDTO = new ImageDataDTO();
        //picture.setTmdbId(id);
        String urlStr = TMDB_IMAGE_URL + "/t/p/" + size + "/" + id;
        URL url;
        try {
            url = new URL(urlStr);
            imageDataDTO.setImageBytesContentType(url.openConnection().getContentType());
            ByteArrayOutputStream output = new ByteArrayOutputStream();

            try (InputStream inputStream = url.openStream()) {
                int n = 0;
                byte[] buffer = new byte[1024];
                while (-1 != (n = inputStream.read(buffer))) {
                    output.write(buffer, 0, n);
                }
            }
            imageDataDTO.setImageBytes(output.toByteArray());
            // TODO : Manage the picture size
            imageDataDTO.setImageSize(size);
            log.debug("Found ImageData ", imageDataDTO);
        } catch (MalformedURLException e) {
            log.error("Error initializing the URL :" + urlStr, e);
        } catch (IOException e) {
            log.error("Error opening the connection :" + urlStr, e);
        }
        return imageDataDTO;
    }

    /*@Transactional(readOnly = true)
    public PictureDTO findOneTMDBPicture(String id) {
        PictureDTO picture = new PictureDTO();
        picture.setTmdbId(id);
        URL url;
        try {
            url = new URL(TMDB_IMAGE_URL + "/t/p/original/" + id);
            picture.setPictureContentType(url.openConnection().getContentType());
            ByteArrayOutputStream output = new ByteArrayOutputStream();

            try (InputStream inputStream = url.openStream()) {
                int n = 0;
                byte[] buffer = new byte[1024];
                while (-1 != (n = inputStream.read(buffer))) {
                    output.write(buffer, 0, n);
                }
            }
            picture.setPicture(output.toByteArray());
            // TODO : Manage the picture size
            picture.setPictureSize("");
            log.debug("Found Picture ", picture);

        } catch (MalformedURLException e) {
            log.error("Error initializing the URL :" + TMDB_IMAGE_URL + "/t/p/original/", e);
        } catch (IOException e) {
            log.error("Error opening the connection :" + TMDB_IMAGE_URL + "/t/p/original/", e);
        }
        return picture;
    }*/
    /*@Transactional(readOnly = true)
    public void findOneTMDBImageBytes(ImageDTO imageDTO) {
        
        String id = imageDTO.getTmdbId();
        URL url;
        
        try {
            url = new URL(TMDB_IMAGE_URL + "/t/p/original/" + id);
            imageDTO.setImageContentType(url.openConnection().getContentType());
            ByteArrayOutputStream output = new ByteArrayOutputStream();

            try (InputStream inputStream = url.openStream()) {
                int n = 0;
                byte[] buffer = new byte[1024];
                while (-1 != (n = inputStream.read(buffer))) {
                    output.write(buffer, 0, n);
                }
            }
            imageDTO.setImage(output.toByteArray());
            // TODO : Manage the picture size
            

        } catch (MalformedURLException e) {
            log.error("Error initializing the URL :" + TMDB_IMAGE_URL + "/t/p/original/", e);
        } catch (IOException e) {
            log.error("Error opening the connection :" + TMDB_IMAGE_URL + "/t/p/original/", e);
        }
        
    }*/

    @Transactional(readOnly = true)
    public TMDBMovieCreditResponse findTMDBCreditsFromMovie(String tmdbMovieId) {
        log.debug("Request to find credits from Movie  {}", tmdbMovieId);
        RestTemplate restTemplate = new RestTemplate();
        TMDBMovieCreditResponse creditsResponse = restTemplate.getForObject(
            tmdbProps.getURLWithAPIKey(TMDB_API_URL + "/movie/" + tmdbMovieId + "/credits?"),
            TMDBMovieCreditResponse.class
        );
        log.debug("Request to find credits from TMDB Movie for id {}, TMDBMovieCreditResponse {} ", tmdbMovieId, creditsResponse);
        return creditsResponse;
    }

    @Transactional(readOnly = true)
    public TMDBMovieImageDTO findTMDBMovieImages(String tmdbMovieId) {
        log.debug("Request to find images from Movie  {}", tmdbMovieId);
        RestTemplate restTemplate = new RestTemplate();
        TMDBMovieImageDTO imagesResponse = restTemplate.getForObject(
            tmdbProps.getURLWithAPIKey(TMDB_API_URL + "/movie/" + tmdbMovieId + "/images?", false),
            TMDBMovieImageDTO.class
        );
        log.debug("Request to find images from TMDB Movie for id {}, TMDBMovieImageDTO {} ", tmdbMovieId, imagesResponse);
        return imagesResponse;
    }

    @Transactional(readOnly = true)
    public TMDBPersonImageDTO findTMDBPersonImages(String personTmdbId) {
        log.debug("Request to find images from Person  {}", personTmdbId);
        RestTemplate restTemplate = new RestTemplate();
        TMDBPersonImageDTO imagesResponse = restTemplate.getForObject(
            tmdbProps.getURLWithAPIKey(TMDB_API_URL + "/person/" + personTmdbId + "/images?", false),
            TMDBPersonImageDTO.class
        );
        log.debug("Request to find images from TMDB Movie for id {}, TMDBMovieImageDTO {} ", personTmdbId, imagesResponse);
        return imagesResponse;
    }
}
