package com.mgoulene.mmdb.tmdb.repository;

import com.mgoulene.mmdb.config.ApplicationProperties;
import com.mgoulene.mmdb.service.dto.MovieDTO;
import com.mgoulene.mmdb.tmdb.domain.TMDBMovie;
import com.mgoulene.mmdb.tmdb.service.dto.TMDBMovieSearchResponse;
import com.mgoulene.mmdb.tmdb.service.mapper.TMDBMovieMapper;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

@Repository
public class TMDBMovieRepository {

    private final Logger log = LoggerFactory.getLogger(TMDBMovieRepository.class);

    private final ApplicationProperties.TMDBProperties tmdbProps;

    private final TMDBMovieMapper tmdbMovieMapper;

    public TMDBMovieRepository(ApplicationProperties appProps, TMDBMovieMapper tmdbMovieMapper) {
        this.tmdbProps = appProps.getTMDB();
        this.tmdbMovieMapper = tmdbMovieMapper;
    }

    public Optional<TMDBMovie> findOneTMDBMovie(String tmdbId) {
        log.debug("Request to find a TMDB Movie for id {}", tmdbId);
        RestTemplate restTemplate = new RestTemplate();
        TMDBMovie tmdbMovieDTO = restTemplate.getForObject(
            tmdbProps.getURLWithAPIKey(TMDBConstants.TMDB_API_URL + "/movie/" + tmdbId + "?"),
            TMDBMovie.class
        );
        log.debug("Request to find a TMDB Movie for id {}, TMDBMovieDTO {}", tmdbId, tmdbMovieDTO);
        return Optional.of(tmdbMovieDTO);
    }

    public List<MovieDTO> searchTMDBMovies(String query) {
        log.debug("Request to search for TMDB Movies for query {}", query);
        RestTemplate restTemplate = new RestTemplate();
        TMDBMovieSearchResponse movies = restTemplate.getForObject(
            tmdbProps.getURLWithAPIKey(TMDBConstants.TMDB_API_URL + "/search/movie?&query=" + query),
            TMDBMovieSearchResponse.class
        );
        return tmdbMovieMapper.toDTO(Arrays.asList(movies.getResults()));
    }
}
